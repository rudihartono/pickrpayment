﻿using PickrPayment.Module.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment.Module.Helper
{
    public class CalculateCreditHelper
    {
        private List<Payment> allPayment;
        private List<Transaction> todayTransaction;
        private DataAdapter.DataReader dataReader;
        public string Query { get; set; }

        public CalculateCreditHelper()
        {
            dataReader = new DataAdapter.DataReader();
            allPayment = new List<Payment>();
            todayTransaction = new List<Transaction>();
        }

        public CalculateCreditHelper(string query)
        {
            dataReader = new DataAdapter.DataReader();
            allPayment = new List<Payment>();
            Query = query;
        }

        public void Run()
        {
            todayTransaction = dataReader.GetListTransaction(ConstantHelper.BONUSQUERY);
            allPayment = dataReader.GetRawData(Query);
            CalculatePayment();
        }

        public async void InsertPickrPaymentHistory(Payment paymentItem)
        {
            decimal bonus = 0;
            decimal? myCredit = paymentItem.Credit;
            try
            {
                List<Transaction> SpidTransaction = todayTransaction.Where(x => x.SpId == paymentItem.SpId && x.ShiftTimeId == paymentItem.ShiftTimeId).OrderBy(y => y.ShiftTimeId).ToList();
                if (SpidTransaction != null && SpidTransaction.Count > 0)
                {
                    List<PickrHourPayment> SpIdDayWorks = new List<PickrHourPayment>();
                    
                    #region every shift time
                    foreach (var pickrHourPay in SpidTransaction.GroupBy(info => info.ShiftTimeId).Select(group => new PickrHourPayment()
                    {
                        SpId = paymentItem.SpId,
                        WorkHour = group.GroupBy(x => x.PickHourId).Count(),
                        TotalKg = group.Sum(y => y.TotalKg).Value,
                        TotalUser = group.GroupBy(z => z.UserId).Count()
                    }))
                    {
                        if (pickrHourPay.WorkHour >= ConstantHelper.MINTOTALHOUR && (pickrHourPay.TotalKg >= ConstantHelper.MINTOTALKG || pickrHourPay.TotalUser >= ConstantHelper.MINTOTALUSER))
                        {
                            bonus += 10000 * (pickrHourPay.WorkHour / 3);
                        }
                    }
                    myCredit += bonus;

                    string pickrPaymentQuery = string.Format(@"INSERT INTO PickrPayment
                                                       (SpId
                                                       ,PickrScheduleId
                                                       ,DayWorkId
                                                       ,DayWork
                                                       ,ShiftTimeId
                                                       ,ShiftName
                                                       ,MyCredit
                                                       ,CreatedDt
                                                       ,CreatedBy)
                                                 VALUES
                                                       ({0}
                                                       ,{1}
                                                       ,{2}
                                                       ,'{3}'
                                                       ,{4}
                                                       ,'{5}'
                                                       ,{6}
                                                       ,GETDATE()
                                                       ,'system')", paymentItem.SpId, paymentItem.PickrScheduleId, paymentItem.DayWorkId, string.Format("{0:yyyy-MM-dd}",paymentItem.DayWork), paymentItem.ShiftTimeId, paymentItem.ShiftName, myCredit);


                    string queryExist = string.Format("SELECT * FROM PickrPayment WHERE SpId = {0} AND PickrScheduleId = {1} AND DayWorkId = {2} AND ShiftTimeId = {3}", paymentItem.SpId, paymentItem.PickrScheduleId, paymentItem.DayWorkId, paymentItem.ShiftTimeId);
                    if (await dataReader.GetPaymentExist(queryExist) == false)
                    {
                        InsertToDB(pickrPaymentQuery);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async void InsertPickrBonus(PickrBonus pickrBonus)
        {
            try
            {
                string pickrBonusQuery = string.Format(@"INSERT INTO ServiceProviderBonus
                                                       (SpId
                                                       ,Bonus
                                                       ,DayWork)
                                                 VALUES
                                                       ({0}
                                                       ,{1})
                                                       ,GETDATE()", pickrBonus.SpId, pickrBonus.Bonus);


                string queryExist = string.Format("SELECT * FROM ServiceProviderBonus WHERE SpId = {0} AND CONVERT(DATE, DayWork, 102) = CONVERT(DATE, GETDATE(), 102)", pickrBonus.SpId);
                if (await dataReader.GetPaymentExist(queryExist) == false)
                {
                    InsertToDB(pickrBonusQuery);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public /*Task<decimal>*/ long GetBonusBySpId(long spid)
        {
            long bonus = 0;

            try {
                List<Transaction> SpidTransaction = todayTransaction.Where(x => x.SpId == spid).OrderBy(y => y.ShiftTimeId).ToList();
                if (SpidTransaction != null && SpidTransaction.Count > 0)
                {
                    List<PickrHourPayment> SpIdDayWorks = new List<PickrHourPayment>();

                    #region first 3 hour works
                    /*var pickHourPay = SpidTransaction.GroupBy(info => info.ShiftTimeId).Select(group => new PickrHourPayment()
                    {
                        SpId = spid,
                        WorkHour = group.GroupBy(x => x.PickHourId).Count(),
                        TotalKg = group.Sum(y => y.TotalKg).Value,
                        TotalUser = group.GroupBy(z => z.UserId).Count()
                    });

                    if (pickHourPay.Sum(x=>x.WorkHour) >= 3 && (pickHourPay.Sum(y=>y.TotalKg) >= 30 || pickHourPay.Sum(z=>z.TotalUser) >= 6))
                    {
                        bonus += 10000;
                    }*/
                    #endregion

                    #region calcuate bonus every 3 hour
                    /*
                    int hourOver = 0;
                    foreach (var pickrHourPay in SpidTransaction.GroupBy(info => info.ShiftTimeId).Select(group => new PickrHourPayment()
                    {
                        SpId = spid,
                        WorkHour = group.GroupBy(x=>x.PickHourId).Count(),
                        TotalKg = group.Sum(y => y.TotalKg).Value,
                        TotalUser = group.GroupBy(z => z.UserId).Count()
                    }))
                    {
                        int workHour = hourOver + pickrHourPay.WorkHour;

                        if (workHour >= 3 && (pickrHourPay.TotalKg >= 27 || pickrHourPay.TotalUser >= 6)){
                            bonus += 10000;
                            if(pickrHourPay.WorkHour > 3)
                            {
                                hourOver = workHour - 3;
                            }
                        }
                    }
                    */
                    #endregion

                    #region every shift time
                    foreach (var pickrHourPay in SpidTransaction.GroupBy(info => info.ShiftTimeId).Select(group => new PickrHourPayment()
                    {
                        SpId = spid,
                        WorkHour = group.GroupBy(x => x.PickHourId).Count(),
                        TotalKg = group.Sum(y => y.TotalKg).Value,
                        TotalUser = group.GroupBy(z => z.UserId).Count()
                    }))
                    {
                        if (pickrHourPay.WorkHour >= ConstantHelper.MINTOTALHOUR && (pickrHourPay.TotalKg >= ConstantHelper.MINTOTALKG || pickrHourPay.TotalUser >= ConstantHelper.MINTOTALUSER))
                        {
                            bonus += 10000 * (pickrHourPay.WorkHour / 3);
                        }
                    }
                    
                    return /*Task.FromResult<decimal>(bonus)*/ bonus;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return /*Task.FromResult<decimal>(bonus)*/ bonus;
        }

        public /*async*/ void CalculatePayment()
        {
            if (allPayment != null && allPayment.Count > 0)
            {
                #region grouping by spid
                List<PaymentGroup> SpIdPayment = new List<PaymentGroup>();

                allPayment = allPayment.OrderBy(x => x.SpId).ToList();
                
                //long spid = allPayment[0].SpId;
                //decimal? creditSum = 0;

                for (int i = 0; i < allPayment.Count; i++)
                {
                    try
                    {
                        long spid = allPayment[i].SpId;
                        long bonus = GetBonusBySpId(spid);
                        PickrBonus pickrBonus = new PickrBonus()
                        {
                            SpId = spid,
                            Bonus = bonus
                        };
                        InsertPickrBonus(pickrBonus);
                    }
                    catch (Exception)
                    {
                        // do nothing
                    }
                    
                    InsertPickrPaymentHistory(allPayment[i]);
                }


                //for (int i = 0; i < allPayment.Count; i++)
                //{
                //    if (allPayment[i].SpId == spid)
                //    {
                //        creditSum += allPayment[i].Credit;
                //        InsertPickrPaymentHistory(allPayment[i]);
                //    }
                //    else
                //    {
                //        decimal lastCredit = dataReader.GetLastCredit(string.Format("SELECT * FROM ServiceProviderCredit WHERE SpId = {0}", spid));
                //        decimal bonus = await GetBonusBySpId(spid);
                //        SpIdPayment.Add(new PaymentGroup()
                //        {
                //            SpId = spid,
                //            Credit = creditSum + lastCredit + bonus,
                //            LastCredit = lastCredit
                //        });
                        
                //        spid = allPayment[i].SpId;
                //        creditSum = allPayment[i].Credit;
                //        InsertPickrPaymentHistory(allPayment[i]);
                //    }

                //    if(i == allPayment.Count - 1)
                //    {
                //        decimal lastCredit = dataReader.GetLastCredit(string.Format("SELECT * FROM ServiceProviderCredit WHERE SpId = {0}", spid));
                //        decimal bonus = await GetBonusBySpId(spid);

                //        SpIdPayment.Add(new PaymentGroup()
                //        {
                //            SpId = spid,
                //            Credit = creditSum + lastCredit + bonus,
                //            LastCredit = lastCredit
                //        });
                //    }
                //}
                #endregion

                //SavePickrCredit(SpIdPayment);
            }
        }

        public void SavePickrCredit(List<PaymentGroup> pickrCredit)
        {
            foreach (PaymentGroup spidPaymenet in pickrCredit)
            {
                //update
                string queryUpdate = string.Format("UPDATE ServiceProviderCredit SET Credit = {0} WHERE SpId = {1}", spidPaymenet.Credit, spidPaymenet.SpId);
                UpdateToDB(queryUpdate);

                string queryInsert = string.Format(@"INSERT INTO ServiceProviderCreditHistory
                                                           (SpId
                                                           ,Credit
                                                           ,LastCredit
                                                           ,CreditTypeId
                                                           ,CountryId
                                                           ,CreatedDt
                                                           ,CreatedBy)
                                                     VALUES
                                                           ({0}
                                                           ,{1}
                                                           ,{2}
                                                           ,6
                                                           ,1
                                                           ,GETDATE()
                                                           ,'system')", spidPaymenet.SpId, spidPaymenet.Credit, spidPaymenet.LastCredit);
                //insert
                InsertToDB(queryInsert);
            }
        }
        
        public void InsertToDB(string query)
        {
            try
            {
                dataReader.Insert(query);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void UpdateToDB(string query)
        {
            try
            {
                dataReader.Update(query);
            }
            catch (Exception ex)
            {

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment.Module.Helper
{
    public class ConstantHelper
    {
        public static double MINTOTALKG = 30;
        public static int MINTOTALUSER = 6;
        public static int MINTOTALHOUR = 3;
        public static string CONN_STRING = @"Data Source=DESKTOP-VFOQ5GL\SQLEXPRESS;Initial Catalog=taptopickdb;Integrated Security=True";
        public static string BONUSQUERY = @"SELECT DISTINCT
    ps.SpId,
    spt.PickrTypeId,
    ps.PickrScheduleId, 
    pdw.DayWorkId, pdw.DayWork, 
    pst.ShiftTimeId, pst.ShiftName, 
    ptw.TimeWork, 
    ph.PickHourId, 
    (
        SELECT COUNT(DISTINCT od.OrderId) 
        FROM [OrderDetail] od 
        WHERE 
            od.PickPlanDate = CONVERT(DATE, GETDATE(),102) 
            AND od.PickHourId = ph.PickHourId   
    ) TotalOrder,
    ou.OrderId,
    u.UserId,
    (
        SELECT SUM(subq1.TotalPrice) TotalPrice FROM                                                                                
        (                                                                                                                         
            SELECT                                                                                                                                                                                                                                                                                                             
                ods.OrderDetailId, 
                ods.OrderId, 
                ods.TotalKg, 
                CASE WHEN spt.PickrTypeId IS NULL THEN 
                    IIF(DATENAME(DW, GETDATE()) = 'Saturday' OR DATENAME(DW, GETDATE()) = 'Monday',
                    ((SELECT PickrPricePerKg FROM [Setting]) * ods.TotalKg),
                    ((SELECT PickrPricePerKgFullTime FROM [Setting]) * ods.TotalKg))
                ELSE
                    (
                        CASE WHEN spt.PickrTypeId = 1 THEN
                            IIF(DATENAME(DW, GETDATE()) = 'Saturday' OR DATENAME(DW, GETDATE()) = 'Monday',
                            ((SELECT PickrPricePerKg FROM [Setting]) * ods.TotalKg),
                            ((SELECT PickrPricePerKgFullTime FROM [Setting]) * ods.TotalKg))
                        ELSE
                            ((SELECT PickrPricePerKg FROM [Setting]) * ods.TotalKg) 
                        END
                    ) 
                END 
                TotalPrice
            FROM 
                [Order] os,                                                                                                        
                [OrderDetail] ods
            WHERE                                                                                                                 
                os.OrderId =  ou.OrderId
                AND ods.OrderId = ou.OrderId                                                                                                  
        ) subq1    
    ) TotalPrice
FROM 
    [PickrSchedule] ps
        INNER JOIN
    [ServiceProvider] sp ON ps.SpId = sp.SpId
        LEFT JOIN 
    [PickrDayWork] pdw ON ps.PickrScheduleId = pdw.PickrScheduleId
        LEFT JOIN 
    [PickrShiftTime] pst ON pdw.DayWorkId = pst.DayWorkId
        LEFT JOIN 
    [PickrTimeWork] ptw ON pst.ShiftTimeId = ptw.ShiftTimeId
        LEFT JOIN 
    [PickHour] ph ON ptw.TimeWork = ph.StartHour
        INNER JOIN 
    (
        SELECT * 
        FROM 
        (
            SELECT OrderId, PickTime PickHour FROM [OrderPick] 
            WHERE 
                PickDate = CONVERT(DATE, GETDATE(),102) 
                UNION 
            SELECT OrderId, DeliveryTime PickHour 
            FROM [OrderDeliver] 
            WHERE 
                DeliveryDate = CONVERT(DATE, GETDATE(),102)
        ) subq1
    ) ou ON ph.PickHourId = ou.PickHour 
        INNER JOIN 
    [Order] o ON ou.OrderId = o.OrderId
        INNER JOIN 
    [User] u ON o.UserId = u.UserId
        INNER JOIN
    [OrderStatus] os ON o.OrderStatusId = os.OrderStatusId
        INNER JOIN
    [Assignment] asg ON asg.SpId = ps.spId
        LEFT JOIN
    [ServiceProviderType] spt ON ps.SpId = spt.SpId
        LEFT JOIN
    [OrderPick] opick ON ps.SpId = opick.SpId
        LEFT JOIN
    [OrderDeliver] odel ON ps.SpId = odel.SpId
    
WHERE 
    --(ps.WeekStart <= CONVERT(DATE, GETDATE(),102) AND ps.WeekEnd >= CONVERT(DATE, GETDATE(),102))
    pdw.DayWork = CONVERT(DATE, GETDATE(),102)
    AND (os.OrderStatusCode = 'CLEANING' OR os.OrderStatusCode = 'DELIVERED')
    --AND (CONVERT(DATE, asg.CreatedDt, 102) = CONVERT(DATE, GETDATE(),102) OR CONVERT(DATE, asg.UpdatedDt, 102) = CONVERT(DATE, GETDATE(),102))
    AND (CONVERT(DATE, opick.PickDate, 102) = CONVERT(DATE, GETDATE(), 102) OR CONVERT(DATE, odel.DeliveryDate, 102) = CONVERT(DATE, GETDATE(), 102))";

        public static string PAYMENTQUERY = @"SELECT
    FinalSubq.SpId,
    FinalSubq.PickrScheduleId,
    FinalSubq.DayWorkId,
	FinalSubq.DayWork,
    FinalSubq.ShiftTimeId,
    FinalSubq.ShiftName,
    SUM(FinalSubq.MyCredit) MyCredit
FROM
(
    SELECT
        PaymentShift.SpId,
        PaymentShift.PickrScheduleId,
        PaymentShift.DayWorkId,
		PaymentShift.DayWork,
        PaymentShift.ShiftTimeId,
        PaymentShift.ShiftName,
        (
            CASE WHEN PaymentShift.TotalPricePerKg > PaymentShift.TotalPriceByUser 
            THEN 
                PaymentShift.TotalPricePerKg 
            ELSE 
                PaymentShift.TotalPriceByUser
            END
        ) MyCredit
    FROM
    (
        SELECT DISTINCT
            PickrPayment.SpId,
            PickrPayment.PickrScheduleId,
            PickrPayment.DayWorkId,
			PickrPayment.DayWork,
            PickrPayment.ShiftTimeId,
            PickrPayment.ShiftName,
            PickrPayment.PickHourId,
            SUM(DISTINCT PickrPayment.TotalPrice) TotalPricePerKg,
            cp.TotalPriceByUser
        FROM
        (
            SELECT DISTINCT
                ps.SpId,
                spt.PickrTypeId,
                ps.PickrScheduleId, 
                pdw.DayWorkId, pdw.DayWork, 
                pst.ShiftTimeId, pst.ShiftName, 
                ptw.TimeWork, 
                ph.PickHourId, 
                (
                    SELECT COUNT(DISTINCT od.OrderId) 
                    FROM [OrderDetail] od 
                    WHERE 
                        od.PickPlanDate = CONVERT(DATE, GETDATE(),102) 
                        AND od.PickHourId = ph.PickHourId   
                ) TotalOrder,
                ou.OrderId,
                u.UserId,
                (
                    SELECT SUM(subq1.TotalPrice) TotalPrice FROM                                                                                
                    (                                                                                                                         
                        SELECT                                                                                                                                                                                                                                                                                                             
                            ods.OrderDetailId, 
                            ods.OrderId, 
                            ods.TotalKg, 
                            CASE WHEN spt.PickrTypeId IS NULL THEN 
                                IIF(DATENAME(DW, GETDATE()) = 'Saturday' OR DATENAME(DW, GETDATE()) = 'Monday',
								((SELECT PickrPricePerKg FROM [Setting]) * ods.TotalKg),
								((SELECT PickrPricePerKgFullTime FROM [Setting]) * ods.TotalKg))
                            ELSE
                                (
                                    CASE WHEN spt.PickrTypeId = 1 THEN
                                        IIF(DATENAME(DW, GETDATE()) = 'Saturday' OR DATENAME(DW, GETDATE()) = 'Monday',
										((SELECT PickrPricePerKg FROM [Setting]) * ods.TotalKg),
										((SELECT PickrPricePerKgFullTime FROM [Setting]) * ods.TotalKg))
                                    ELSE
                                        ((SELECT PickrPricePerKg FROM [Setting]) * ods.TotalKg) 
                                    END
                                ) 
                            END 
                            TotalPrice
                        FROM 
                            [Order] os,                                                                                                        
                            [OrderDetail] ods
                        WHERE                                                                                                                 
                            os.OrderId =  ou.OrderId
                            AND ods.OrderId = ou.OrderId                                                                                                  
                    ) subq1    
                ) TotalPrice
            FROM 
                [PickrSchedule] ps
                    INNER JOIN
                [ServiceProvider] sp ON ps.SpId = sp.SpId
                    LEFT JOIN 
                [PickrDayWork] pdw ON ps.PickrScheduleId = pdw.PickrScheduleId
                    LEFT JOIN 
                [PickrShiftTime] pst ON pdw.DayWorkId = pst.DayWorkId
                    LEFT JOIN 
                [PickrTimeWork] ptw ON pst.ShiftTimeId = ptw.ShiftTimeId
                    LEFT JOIN 
                [PickHour] ph ON ptw.TimeWork = ph.StartHour
                    INNER JOIN 
                (
                    SELECT * 
                    FROM 
                    (
                        SELECT OrderId, PickTime PickHour FROM [OrderPick] 
                        WHERE 
                            PickDate = CONVERT(DATE, GETDATE(),102) 
                            UNION 
                        SELECT OrderId, DeliveryTime PickHour 
                        FROM [OrderDeliver] 
                        WHERE 
                            DeliveryDate = CONVERT(DATE, GETDATE(),102)
                    ) subq1
                ) ou ON ph.PickHourId = ou.PickHour 
                    INNER JOIN 
                [Order] o ON ou.OrderId = o.OrderId
                    INNER JOIN 
                [User] u ON o.UserId = u.UserId
                    INNER JOIN
                [OrderStatus] os ON o.OrderStatusId = os.OrderStatusId
                    INNER JOIN
                [Assignment] asg ON asg.SpId = ps.spId
                    LEFT JOIN
                [ServiceProviderType] spt ON ps.SpId = spt.SpId
                    LEFT JOIN
                [OrderPick] opick ON ps.SpId = opick.SpId
                    LEFT JOIN
                [OrderDeliver] odel ON ps.SpId = odel.SpId
                
            WHERE 
                --(ps.WeekStart <= CONVERT(DATE, GETDATE(),102) AND ps.WeekEnd >= CONVERT(DATE, GETDATE(),102))
                pdw.DayWork = CONVERT(DATE, GETDATE(),102)
                AND (os.OrderStatusCode = 'CLEANING' OR os.OrderStatusCode = 'DELIVERED')
                --AND (CONVERT(DATE, asg.CreatedDt, 102) = CONVERT(DATE, GETDATE(),102) OR CONVERT(DATE, asg.UpdatedDt, 102) = CONVERT(DATE, GETDATE(),102))
                AND (CONVERT(DATE, opick.PickDate, 102) = CONVERT(DATE, GETDATE(), 102) OR CONVERT(DATE, odel.DeliveryDate, 102) = CONVERT(DATE, GETDATE(), 102))
        ) PickrPayment
            INNER JOIN
        (
            SELECT 
                UserUnique.PickHourId,
                COUNT(DISTINCT UserUnique.UserId) UserUnique,
                CASE WHEN UserUnique.PickrTypeId IS NULL THEN 
                    IIF(DATENAME(DW, GETDATE()) = 'Saturday' OR DATENAME(DW, GETDATE()) = 'Monday', 
					((SELECT CustomerPricePerHour FROM [Setting])*COUNT(DISTINCT UserUnique.UserId)),
					((SELECT CustomerPricePerHourFullTime FROM [Setting])*COUNT(DISTINCT UserUnique.UserId)))
                ELSE
                    (
                        CASE WHEN UserUnique.PickrTypeId = 1 THEN
                            IIF(DATENAME(DW, GETDATE()) = 'Saturday' OR DATENAME(DW, GETDATE()) = 'Monday',
							((SELECT CustomerPricePerHour FROM [Setting])*COUNT(DISTINCT UserUnique.UserId)),
							((SELECT CustomerPricePerHourFullTime FROM [Setting])*COUNT(DISTINCT UserUnique.UserId)))
                        ELSE
                            ((SELECT CustomerPricePerHour FROM [Setting])*COUNT(DISTINCT UserUnique.UserId))
                        END
                    ) 
                END 
                TotalPriceByUser
            FROM
            (
                SELECT 
                    ps.PickrScheduleId, 
                    spt.PickrTypeId,
                    pdw.DayWorkId, 
                    pst.ShiftTimeId, pst.ShiftName, 
                    ptw.TimeWork, 
                    ph.PickHourId, 
                    u.UserId
                FROM 
                    [PickrSchedule] ps
                        LEFT JOIN 
                    [PickrDayWork] pdw ON ps.PickrScheduleId = pdw.PickrScheduleId
                        LEFT JOIN 
                    [PickrShiftTime] pst ON pdw.DayWorkId = pst.DayWorkId
                        LEFT JOIN 
                    [PickrTimeWork] ptw ON pst.ShiftTimeId = ptw.ShiftTimeId
                        LEFT JOIN 
                    [PickHour] ph ON ptw.TimeWork = ph.StartHour
                        INNER JOIN 
                    (
                        SELECT * 
                        FROM 
                        (
                            SELECT OrderId, PickTime PickHour FROM [OrderPick] 
                            WHERE 
                                PickDate = CONVERT(DATE, GETDATE(),102) 
                                UNION 
                            SELECT OrderId, DeliveryTime PickHour 
                            FROM [OrderDeliver] 
                            WHERE
                                DeliveryDate = CONVERT(DATE, GETDATE(),102)
                        ) subq1
                    ) ou ON ph.PickHourId = ou.PickHour 
                        INNER JOIN 
                    [Order] o ON ou.OrderId = o.OrderId
                        INNER JOIN 
                    [User] u ON o.UserId = u.UserId
                        LEFT JOIN
                    [ServiceProviderType] spt ON ps.SpId = spt.SpId
                        LEFT JOIN
                    [OrderPick] opick ON ps.SpId = opick.SpId
                        LEFT JOIN
                    [OrderDeliver] odel ON ps.SpId = odel.SpId
                WHERE 
                    --(ps.WeekStart <= CONVERT(DATE, GETDATE(),102) AND ps.WeekEnd >= CONVERT(DATE, GETDATE(),102))
                    pdw.DayWork = CONVERT(DATE, GETDATE(),102)
                    AND (CONVERT(DATE, opick.PickDate, 102) = CONVERT(DATE, GETDATE(), 102) OR CONVERT(DATE, odel.DeliveryDate, 102) = CONVERT(DATE, GETDATE(), 102))
            ) UserUnique
            GROUP BY UserUnique.PickHourId, UserUnique.PickrTypeId
        ) cp ON PickrPayment.PickHourId = cp.PickHourId
        GROUP BY PickrPayment.SpId, PickrPayment.PickrScheduleId, PickrPayment.DayWorkId, PickrPayment.DayWork, PickrPayment.ShiftTimeId, PickrPayment.ShiftName, cp.TotalPriceByUser, PickrPayment.PickHourId
    ) PaymentShift
) FinalSubq
GROUP BY FinalSubq.SpId, FinalSubq.PickrScheduleId, FinalSubq.DayWorkId, FinalSubq.DayWork, FinalSubq.ShiftTimeId, FinalSubq.ShiftName
";
    }
}

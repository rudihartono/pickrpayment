﻿using PickrPayment.Module.Helper;
using PickrPayment.Module.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment.Module.DataAdapter
{
    public class DataReader
    {
        private SqlConnection conn;
        private SqlCommand cmd;

        public List<Payment> GetRawData(string query)
        {
            List<Payment> result = new List<Payment>();
            try
            {
                using (conn = new SqlConnection(Properties.Settings.Default.taptopickconnect))
                {
                    conn.Open();
                    using (cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            Console.WriteLine("connected.");
                            // Check is the reader has any rows at all before starting to read.
                            if (reader.HasRows)
                            {
                                // Read advances to the next row.
                                while (reader.Read())
                                {
                                    result.Add(new Payment()
                                    {
                                        SpId = reader.GetInt64(0),
                                        PickrScheduleId = reader.GetInt64(1),
                                        DayWorkId = reader.GetInt64(2),
                                        DayWork = reader.GetDateTime(3),
                                        ShiftTimeId = reader.GetInt64(4),
                                        ShiftName = reader.GetString(5),
                                        Credit = Convert.ToDecimal(reader.GetDouble(6))
                                    });
                                }
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }

        public List<Transaction> GetListTransaction(string query)
        {
            List<Transaction> result = new List<Transaction>();
            try
            {
                using (conn = new SqlConnection(Properties.Settings.Default.taptopickconnect))
                {
                    conn.Open();
                    using (cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Check is the reader has any rows at all before starting to read.
                            if (reader.HasRows)
                            {
                                // Read advances to the next row.
                                while (reader.Read())
                                {
                                    decimal totalPrice = 0;
                                    double totalKg = 0;

                                    if(!reader.IsDBNull(11))
                                    {
                                        totalPrice = Convert.ToDecimal(reader.GetDouble(11));
                                    }

                                    if (!reader.IsDBNull(12))
                                    {
                                        totalKg = reader.GetDouble(12);
                                    }
                                    
                                    result.Add(new Transaction()
                                    {
                                        SpId = reader.GetInt64(0),
                                        PickrScheduleId = reader.GetInt64(1),
                                        DayWorkId = reader.GetInt64(2),
                                        DayWork = reader.GetDateTime(3),
                                        ShiftTimeId = reader.GetInt64(4),
                                        ShiftName = reader.GetString(5),
                                        TimeWork = reader.GetTimeSpan(6),
                                        PickHourId = reader.GetInt32(7),
                                        TotalOrder = reader.GetInt32(8),
                                        OrderId = reader.GetInt64(9),
                                        UserId = reader.GetInt64(10),
                                        TotalPrice = totalPrice,
                                        TotalKg = totalKg
                                    });
                                }
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }

        public decimal GetLastCredit(string query)
        {
            decimal lastCredit = 0;
            try
            {
                using (conn = new SqlConnection(Properties.Settings.Default.taptopickconnect))
                {
                    conn.Open();
                    using (cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Check is the reader has any rows at all before starting to read.
                            if (!reader.Read())
                                return 0;

                            lastCredit = reader.GetDecimal(2);
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                return lastCredit;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return 0;
        }
        public Task<bool> GetPaymentExist(string query)
        {
            bool success = false;
            try
            {
                using (conn = new SqlConnection(Properties.Settings.Default.taptopickconnect))
                {
                    conn.Open();
                    using (cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Check is the reader has any rows at all before starting to read.
                            if (!reader.Read())
                                success = false;
                            else
                                success = true;
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return Task.FromResult<bool>(success);
        }
        public void Insert(string query)
        {
            try
            {
                using (conn = new SqlConnection(Properties.Settings.Default.taptopickconnect))
                {
                    conn.Open();
                    using (cmd = new SqlCommand(query, conn))
                    {
                        Console.WriteLine("Commands executed! Total rows affected are " + cmd.ExecuteNonQuery());
                    }

                    conn.Close();
                    conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Update(string query)
        {
            try
            {
                using (conn = new SqlConnection(Properties.Settings.Default.taptopickconnect))
                {
                    conn.Open();
                    using (cmd = new SqlCommand(query, conn))
                    {
                        Console.WriteLine("Commands executed! Total rows updated are " + cmd.ExecuteNonQuery());
                    }

                    conn.Close();
                    conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
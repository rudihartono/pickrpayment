﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment.Module.Model
{
    public class PickrHourPayment
    {
        public long SpId { get; set; }
        public int WorkHour { get; set; }
        public int TotalUser { get; set; }
        public double TotalKg { get; set; }
        public decimal Bonus { get; set; }
    }
}

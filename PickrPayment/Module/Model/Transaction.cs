﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment.Module.Model
{
    public class Transaction
    {
        public long SpId { get; set; }
        public long PickrScheduleId { get; set; }
        public long DayWorkId { get; set; }
        public Nullable<System.DateTime> DayWork { get; set; }
        public long ShiftTimeId { get; set; }
        public string ShiftName { get; set; }
        public Nullable<System.TimeSpan> TimeWork { get; set; }
        public int PickHourId { get; set; }
        public long TotalOrder { get; set; }
        public long OrderId { get; set; }
        public long UserId { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<double> TotalKg { get; set; }
    }
}

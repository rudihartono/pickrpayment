﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment.Module.Model
{
    public class Payment
    {
        public long SpId { get; set; }
        public long PickrScheduleId { get; set; }
        public long DayWorkId { get; set; }
        public Nullable<System.DateTime> DayWork { get; set; }
        public long ShiftTimeId { get; set; }
        public string ShiftName { get; set; }
        public Nullable<decimal> Credit { get; set; }
    }
}

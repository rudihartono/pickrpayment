﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment.Module.Model
{
    public class PaymentGroup
    {
        public long SpId { get; set; }
        public Nullable<decimal> Credit { get; set; }
        public Nullable<decimal> LastCredit { get; set; }
    }
}

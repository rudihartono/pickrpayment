﻿using PickrPayment.Module.DataAdapter;
using PickrPayment.Module.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickrPayment
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CalculateCreditHelper machine = new CalculateCreditHelper(ConstantHelper.PAYMENTQUERY);

            machine.Run();
        }
    }
}
